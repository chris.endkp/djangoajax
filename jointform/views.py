from django.shortcuts import render

# Create your views here.

from django.views.generic import FormView
from .forms import JoinForm
from django.http import JsonResponse
from .mixins import AjaxFormMixin


class JointFormView(AjaxFormMixin, FormView):
    form_class = JoinForm
    template_name = 'jointform/forms/ajax.html'
    success_url = '/form-success/'


