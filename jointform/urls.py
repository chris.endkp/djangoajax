from django.urls import path
from . import views

urlpatterns = [
    path('', views.JointFormView.as_view(), name='joint-form'),
]